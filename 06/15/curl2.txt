python docopttown2.py "https://github.com/cobrateam/formulae/blob/master/swig.rb?slide=1" \
 -H "Accept-Encoding: gzip,deflate,sdch" \
 -H "Host: github.com" \
 -H "Accept-Language: en-US,en;q=0.8" \
 -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36" \
 -H "Accept: */*" \
 -H "Referer: https://github.com/cobrateam/formulae/blob/master/swig.rb" \
 -H "X-Requested-With: XMLHttpRequest" \
 -H "Connection: keep-alive" \
 -H "X-PJAX: true"

